# Trustiq (Tox Rust Integration for Qt/QML)

Rust implementation of the [Toxer](https://gitlab.com/toxer/toxer-core) engine for use in Qt/QML applications.
The engine implements a [Tox Client](https://github.com/tox-rs/tox) suitable as a common basis for Qt/QML frontends.

## For developers and contributors

### Initialize the development environment

After cloning this repository initialize the directory.

```bash
make init
```

This will clone the `toxer-desktop` repository containing the QML files and assets (such as icons etc.).

### Build and run the trustiq application.

Note that some components are not completed yet and require some work.
While not essentially required you can modfy the theme colors by placing the following code snippet after line 12 in toxer-desktop/qml/Main.qml (or anywhere in the `ApplicationWindow` component).

```qml
  Component.onCompleted {
    Style.color.accent = "#ddd";
    Style.color.base = "#336";
    Style.color.text = "#888";
  }
```

Then build and run the application.

```bash
QT_LIBRARY_PATH=/usr/lib/qt6 QT_INCLUDE_PATH=/usr/include/qt6 cargo run
```
