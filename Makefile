toxer-desktop:
	git clone https://gitlab.com/toxer/toxer-desktop.git --branch=master

init: toxer-desktop
	$(info "Cloning required Git repositories…")

run: init
	cargo run
