use qmetaobject::prelude::*;

mod app_assets;
mod tox_profile;
mod toxer;
mod toxer_desktop;

fn main() {
    qmetaobject::log::init_qt_to_rust();
    app_assets::initialize();
    toxer::register_qml_types();
    toxer_desktop::register_qml_types();

    let mut engine = QmlEngine::new();
    let app_interface = toxer::AppInterface::default();
    let app_interface = engine.new_qobject(app_interface);
    let app_theme = toxer_desktop::Theme::default();
    let app_theme = engine.new_qobject(app_theme);

    engine.set_property("Toxer".into(), app_interface.to_variant());
    engine.set_property("Style".into(), app_theme.to_variant());
    engine.add_import_path("toxer-desktop/qml".into());
    engine.load_file("toxer-desktop/qml/Main.qml".into());
    engine.exec();
}
