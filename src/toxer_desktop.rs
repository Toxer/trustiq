use cstr::cstr;
use qmetaobject::{prelude::*, qreal, QMetaType, QRectF};
use std::cell::RefCell;

#[allow(non_snake_case)]
#[derive(Default, QObject)]
struct Clipboard {
    base: qt_base_class!(trait QObject),
    // clipborad: /* TODO  */
    text: qt_method!(
        fn text(&self) -> QString {
            // TODO:
            "".into()
        }
    ),
    setText: qt_method!(
        fn setText(&self, text: QString) {
            // TODO: self.clipboard.set_text()
            todo!();
        }
    ),
}

#[allow(non_snake_case)]
#[derive(Default, QObject)]
struct ThemeColors {
    base: qt_base_class!(trait QObject),

    accent: qt_property!(QColor; NOTIFY accentChanged),
    accentChanged: qt_signal!(),
    main: qt_property!(QColor; ALIAS base NOTIFY mainChanged),
    mainChanged: qt_signal!(),
    mainHue: qt_property!(QColor; ALIAS baseHue NOTIFY mainHueChanged),
    mainHueChanged: qt_signal!(),
    mainLightness: qt_property!(QColor; ALIAS baseLightness NOTIFY mainLightnessChanged),
    mainLightnessChanged: qt_signal!(),
    mainSaturation: qt_property!(QColor; ALIAS baseSaturation NOTIFY mainSaturationChanged),
    mainSaturationChanged: qt_signal!(),
    contrast: qt_property!(QColor; NOTIFY contrastChanged),
    contrastChanged: qt_signal!(),
    hover: qt_property!(QColor; NOTIFY hoverChanged),
    hoverChanged: qt_signal!(),
    lightTheme: qt_property!(bool; NOTIFY lightThemeChanged),
    lightThemeChanged: qt_signal!(),
    nomatch: qt_property!(QColor; NOTIFY nomatchChanged),
    nomatchChanged: qt_signal!(),
    text: qt_property!(QColor; NOTIFY textChanged),
    textChanged: qt_signal!(),
    valid: qt_property!(QColor; NOTIFY validChanged),
    validChanged: qt_signal!(),
}

#[allow(non_snake_case)]
#[derive(Default, QObject)]
pub struct Theme {
    base: qt_base_class!(trait QObject),

    color: qt_property!(RefCell<ThemeColors>; NOTIFY colorsChanged),
    colorsChanged: qt_signal!(),

    fontSize: qt_property!(qreal; NOTIFY fontSizeChanged),
    fontSizeChanged: qt_signal!(),
}

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, QEnum)]
#[repr(C)]
pub enum AppLayout {
    #[default]
    Slim,
    Split,
}
impl QMetaType for AppLayout {}

#[allow(non_snake_case)]
#[derive(Default, QObject)]
struct UiSettings {
    base: qt_base_class!(trait QObject),

    app_layout: qt_property!(u8; NOTIFY app_layout_changed),
    fullscreen: qt_property!(bool; NOTIFY fullscreen_changed),
    geometry: qt_property!(QRectF; NOTIFY geometry_changed),

    app_layout_changed: qt_signal!(layout_enum: u8),
    fullscreen_changed: qt_signal!(fullscreen: bool),
    geometry_changed: qt_signal!(rect: QRectF),
}

pub fn register_qml_types() {
    // Toxer.Desktop
    qml_register_type::<Clipboard>(cstr!("Toxer.Desktop"), 1, 0, cstr!("Clipboard"));

    // Toxer.Settings
    qml_register_type::<UiSettings>(cstr!("Toxer.Settings"), 1, 0, cstr!("UiSettings"));
    qml_register_enum::<AppLayout>(cstr!("Toxer.Settings"), 1, 0, cstr!("UiSettings.AppLayout"));
}
