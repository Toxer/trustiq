#![allow(unused)]

use standard_paths::{default_paths, LocationType};
use std::{
    fs, io,
    path::{Path, PathBuf},
};
// use tox::core;

pub fn activate(name: &str, password: &str) {
    println!("TODO -> activate profile {name}…");
    //   let old = active_profile();
    //   // activate(name, password);
    //   let p = active_Profile();
    //   if old != p {
    //     // p->set_file_notifier(new FileTransfer(*this));
    //     emit profileChanged();
    //   }
}

pub fn create(name: &str, password: &str, tox_alias: &str) {
    let dir = location();
    let dir: &Path = Path::new(&dir);
    // QDir().mkpath(Toxer::profilesDir());
    // QFile f(Toxer::profilesDir() % QString::fromUtf8("/%1.tox").arg(name));

    // if (f.exists()) {
    //   qWarning("Existing Tox profile found at \"%s\"",
    //            qUtf8Printable(f.fileName()));
    //   return;
    // }

    // if (f.open(QFile::WriteOnly)) {
    //   auto t = createTox({});
    //   if (t) {
    //     // set tox user name
    //     auto c_alias = toxAlias.toUtf8();
    //     tox_self_set_name(t,
    //                       Toxer::to_tox(c_alias.constData()),
    //                       static_cast<size_t>(c_alias.size()),
    //                       nullptr);

    //     write  tox file
    //     auto size = tox_get_savedata_size(t);
    //     auto data = new char[size];
    //     tox_get_savedata(t, Toxer::to_tox(data));
    //     auto encrypted = Toxer::encrypt(name, data, size, password);
    //     if (encrypted.isEmpty()) {
    //       qWarning("Failed to encrypt Tox Profile!");
    //     } else {
    //       f.write(encrypted);
    //     }
    //     delete[] data;
    //     tox_kill(t);
    //   }
    // } else {
    //   qWarning("Failed to create Tox profile %s.\nError message: %s",
    //            qUtf8Printable(name),
    //            qUtf8Printable(f.errorString()));
    // }
}

pub fn list_available() -> Result<Vec<PathBuf>, io::Error> {
    let dir = crate::tox_profile::location();
    let dir: &Path = Path::new(&dir);
    let result = fs::read_dir(dir)?
        .into_iter()
        .filter(|e| e.is_ok())
        .map(|e| e.unwrap().path())
        .filter(|e| e.is_file() && e.extension().is_some_and(|e| e == "tox"))
        .map(|e| {
            e.file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .strip_suffix(".tox")
                .unwrap()
                .into()
        })
        .collect();
    Ok(result)
}

pub fn location() -> String {
    let sp = default_paths!();
    format!(
        "{}/tox",
        sp.writable_location(LocationType::GenericConfigLocation)
            .unwrap()
            .display()
    )
}
