use cstr::cstr;
use qmetaobject::{prelude::*, qml_register_enum, qml_register_type, QMetaType, QStringList};
use std::str::Bytes;

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, QEnum)]
#[repr(C)]
pub enum Proxy {
    #[default]
    None,
    HTTP,
    SOCKS5,
}
impl QMetaType for Proxy {}

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, QEnum)]
#[repr(C)]
pub enum ConnectionType {
    #[default]
    None,
    TCP,
    UDP,
}
impl QMetaType for ConnectionType {}

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, QEnum)]
#[repr(C)]
pub enum UserStatus {
    #[default]
    Unknown,
    Ready,
    Away,
    Busy,
}
impl QMetaType for UserStatus {}

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, QEnum)]
#[repr(C)]
pub enum Transport {
    Resume,
    #[default]
    Pause,
    Cancel,
}
impl QMetaType for Transport {}

// TODO: implement these
#[derive(Default, QObject)]
pub struct ToxAddressValidator {
    base: qt_base_class!(trait QObject),
}

#[allow(non_snake_case)]
#[derive(Default, QObject)]
pub struct AppInterface {
    base: qt_base_class!(trait QObject),

    // profile(s)
    profile: qt_property!(QString; NOTIFY profileChanged),
    profileChanged: qt_signal!(),

    // avatars
    avatarRequest: qt_signal!(friendNo: i32, fileNo: i32),

    // file api
    fileCancelled: qt_signal!(friendNo: i32, fileNo: i32),
    fileChunk: qt_signal!(friendNo: i32, fileNo: i32, size: i32),
    fileFinished: qt_signal!(friendNo: i32, fileNo: i32),
    filePaused: qt_signal!(friendNo: i32, fileNo: i32),
    fileResumed: qt_signal!(friendNo: i32, fileNo: i32),
    fileRequest: qt_signal!(friendNo: i32, fileNo: i32, name: QString, size: i32),

    // qml functions
    activateProfile: qt_method!(
        fn activateProfile(&self, name: QString, password: QString) {
            crate::tox_profile::activate(&name.to_string(), &password.to_string());
        }
    ),
    availableProfiles: qt_method!(
        fn availableProfiles(&self) -> QStringList {
            available_profiles_qstringlist()
        }
    ),
    hasProfile: qt_method!(
        fn hasProfile(&self) -> bool {
            false // TODO: implement profile in rust
        }
    ),
    toxVersionString: qt_method!(
        fn toxVersionString(&self) -> QString {
            tox::crate_version().into()
        }
    ),
}

#[allow(unused)]
trait IToxFileNotifier {
    fn on_avatar_request(friend_no: u32, file_no: u32, file_size: u64) {}
    fn on_cancelled(friend_no: u32, file_no: u32) {}
    fn on_chunk(friend_no: u32, file_no: u32, file_pos: u64, data: &Bytes, chunk_size: u64) {}
    fn on_file_request(friend_no: u32, file_no: u32, file_name: &String, file_size: u64) {}
    fn on_file_transfer(friend_no: u32, file_no: u32, path: &String) {}
    fn on_finished(friend_no: u32, file_no: u32) {}
    fn on_next_chunk(friend_no: u32, file_no: u32, pos: u64, len: u64) {}
    fn on_paused(friend_no: u32, file_no: u32) {}
    fn on_resumed(friend_no: u32, file_no: u32) {}
}

#[allow(unused)]
trait IToxFriendNotifier {
    fn on_name_changed(friend_no: u32, name: &QString) {}
    fn on_status_message_changed(friend_no: u32, message: &QString) {}
    fn on_status_changed(friend_no: u32, status: UserStatus) {}
    fn on_is_online_changed(friend_no: u32, online: bool) {}
    fn on_is_typing_changed(friend_no: u32, typing: bool) {}
    fn on_message(friend_no: u32, message: &QString) {}
}

#[allow(unused)]
trait IToxFriendsNotifier {
    fn on_added(index: u32) {}
    fn on_deleted(index: u32) {}
}

#[allow(unused)]
trait IToxProfileNotifier {
    fn on_is_online_changed(&self, online: bool);
    fn on_status_message_changed(&self, message: QString);
    fn on_status_changed(&self, status: UserStatus);
    fn on_friend_request(&self, pk: QByteArray, msg: QString);
}

#[allow(non_snake_case)]
#[derive(Default, QObject)]
pub struct ToxProfileQuery {
    base: qt_base_class!(trait QObject),

    name: qt_method!(
        fn name(&self) -> QString {
            todo!();
        }
    ),
    userName: qt_method!(
        fn userName(&self) -> QString {
            todo!();
        }
    ),
    setUsername: qt_method!(
        fn setUsername(&self, new_value: QString) {
            todo!();
        }
    ),
    statusMessage: qt_method!(
        fn statusMessage(&self) -> QString {
            todo!()
        }
    ),
    setStatusMessage: qt_method!(
        fn setStatusMessage(&self, new_value: String) {
            todo!();
        }
    ),
    addressStr: qt_method!(
        fn addressStr(&self) -> QString {
            todo!();
        }
    ),
    publicKeyStr: qt_method!(
        fn publicKeyStr(&self) -> QString {
            todo!();
        }
    ),
    nospam: qt_method!(
        fn nospam(&self) -> u32 {
            todo!();
        }
    ),
    isMe: qt_method!(
        fn isMe(&self, pk: QString) -> bool {
            todo!();
        }
    ),
    isOnline: qt_method!(
        fn isOnline(&self) -> bool {
            todo!();
        }
    ),
    status: qt_method!(
        fn status(&self) -> UserStatus {
            todo!();
        }
    ),
    statusInt: qt_method!(
        fn statusInt(&self) -> u8 {
            todo!();
        }
    ),
    setStatus: qt_method!(
        fn setStatus(&self, new_value: u8) {
            todo!()
        }
    ),
    inviteFriend: qt_method!(
        fn inviteFriend(&self, toxId: QString, message: QString) {
            todo!()
        }
    ),
    userNameChanged: qt_signal!(userName: QString),
    isOnlineChanged: qt_signal!(online: bool),
    statusMessageChanged: qt_signal!(statusMessage: QString),
    statusChanged: qt_signal!(status: UserStatus),
    friendRequest: qt_signal!(pk: QString, message: QString),
}
impl IToxProfileNotifier for ToxProfileQuery {
    fn on_is_online_changed(&self, online: bool) {
        self.isOnlineChanged(online);
    }
    fn on_status_message_changed(&self, message: QString) {
        self.statusMessageChanged(message);
    }
    fn on_status_changed(&self, status: UserStatus) {
        self.statusChanged(status);
    }
    fn on_friend_request(&self, pk: QByteArray, msg: QString) {
        self.friendRequest(pk.to_string().into(), msg);
    }
}

#[derive(Default, QObject)]
pub struct ToxFriend {
    base: qt_base_class!(trait QObject),
}

#[derive(Default, QObject)]
pub struct ToxFriends {
    base: qt_base_class!(trait QObject),
}

pub struct Profile {
    active: bool,
    // fn active_profile(&self) {}
}

pub fn register_qml_types() {
    // Toxer
    qml_register_type::<ToxAddressValidator>(cstr!("Toxer"), 1, 0, cstr!("ToxAddressValidator"));
    qml_register_type::<ToxProfileQuery>(cstr!("Toxer"), 1, 0, cstr!("ToxProfileQuery"));
    qml_register_type::<ToxFriend>(cstr!("Toxer"), 1, 0, cstr!("ToxFriend"));
    qml_register_type::<ToxFriends>(cstr!("Toxer"), 1, 0, cstr!("ToxFriends"));

    // Toxer.Types
    qml_register_enum::<ConnectionType>(cstr!("Toxer.Types"), 1, 0, cstr!("ConnectionType"));
    qml_register_enum::<Proxy>(cstr!("Toxer.Types"), 1, 0, cstr!("Proxy"));
    qml_register_enum::<UserStatus>(cstr!("Toxer.Types"), 1, 0, cstr!("UserStatus"));
    qml_register_enum::<Transport>(cstr!("Toxer.Types"), 1, 0, cstr!("Transport"));

    // Toxer.Settings
    // qml_register_type<ToxSettings>(cstr!("Toxer.Settings"), 1, 0, cstr!("ToxSettings"));
}

pub fn available_profiles_qstringlist() -> QStringList {
    let mut out = QStringList::new();
    crate::tox_profile::list_available()
        .unwrap()
        .into_iter()
        .for_each(|e| out.push(e.to_str().unwrap().into()));
    out
}
